#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <iterator>
using namespace std;
inline long int get_long_int(long int n=0)
{
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();
	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
int main(int argc, char const *argv[])
{
	long int n=get_int();
	long int k=get_int();
	long int narr[n];
	for (long int i = 0; i < n; ++i)
	{
		narr[i]=get_int();
	}
	if (k==0)
	{
		for (long int i = 0; i < n; ++i)
			printf("%lld ", narr[i]);
		return 0;
	}
	long int maxi=*max_element(narr,narr+n);
	long int mini=*min_element(narr,narr+n);
	long int narra[n];
	for (long int i = 0; i < n; ++i)
	{
		narr[i]=maxi-narr[i];
		narra[i]=maxi-mini-narr[i];
	}
	long int* p;
	if (k%2)
		p=narr;
	else
		p=narra;
	for (int i = 0; i < n; ++i)
		printf("%lld ", *(p+i));
	return 0;
}