#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
import sys
sys.stdin = open('/tmp/spojtest.in', 'r')


def main():
    cur = (1, 1)
    for _ in range(int(sys.stdin.read())):
        cur = (cur[1], cur[0] + cur[1])
    print(cur[1])




main()
