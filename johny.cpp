using namespace std;
#include <stdio.h>
#include <iostream>
inline int get_int(int n=0)
{
	
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();

	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
inline long long int get_long_int(long long int n=0)
{
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();

	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
void swap(long long int &a, long long int &b)
{
	long long int c=a;
	a=b;
	b=c;
}
int main(int argc, char const *argv[])
{
	int t=get_int();
	while(t--)
	{
		int n=get_int();
		long long int lengths[101];
		for (int i = 0; i < n; ++i)
			lengths[i]=get_long_int();
		int k=get_int()-1;
		swap(lengths[0],lengths[k]);
		int j=1;
		for (int i = 1; i < n; ++i)
		{
			if (lengths[i] < lengths[0])
			{
				swap(lengths[i], lengths[j]);
				j++;
			}
		}
		printf("%d\n", j);
	}	
	return 0;
}