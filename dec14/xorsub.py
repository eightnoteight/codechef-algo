from sys import stdin, stdout

def maximumxor(a, ans, n):
    a.sort(reverse=True)
    i = 0
    maxbit = (len(bin(a[0])) - 3)
    while maxbit >= 0 and i < n:
        while not (a[i] & (1 << maxbit)):
            for x in xrange(i, n):
                if a[x] & (1 <<  maxbit):
                    a[x], a[i] = a[i], a[x]
                    maxbit += 1
                    break
            maxbit -= 1
            if maxbit < 0:
                break
        if maxbit < 0:
            break
        j = i + 1
        while j < n:
            if a[j] & (1 << maxbit):
                a[j] ^= a[i]
            j += 1
        maxbit -= 1
        i += 1
    for x in a:
        if ans^x > ans:
            ans ^= x
    return ans


inp = stdin.readlines()
inpi = 1
for _ in xrange(int(inp[0])):
    n, k = map(int, inp[inpi].split())
    a = map(int, inp[inpi + 1].split())
    inpi += 2
    a.sort(reverse=True)
    maxbit = len(bin(a[0])) - 3
    if maxbit > len(bin(k)) - 3:
        i = 0
        while i < n and (a[i] & (1 << maxbit)):
            a[i] = a[i] ^ k
            i += 1
        stdout.write(str(maximumxor(a, 0, n)))
    else:
        stdout.write(str(maximumxor(a, k, n)))
    stdout.write('\n')
