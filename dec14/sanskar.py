for _ in xrange(int(raw_input())):
    n, k = map(int, raw_input().split())
    arr = map(int, raw_input().split())
    arrsum = sum(arr)
    if not (arrsum % k):
        print 'no'
        continue
    t = arrsum // k
    arr.sort()
    if arr[-1] > t:
        print 'no'
        continue

