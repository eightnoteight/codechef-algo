#include <cstdio>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
using namespace std;
inline int get_int(int n=0)
{
	
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();

	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
inline long long int get_long_int(long long int n=0)
{
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-'){
		sign=-1;
		c=getchar_unlocked();
	}
	while(c>='0'&&c<='9'){
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
int main(int argc, char const *argv[])
{
	int n=get_int();
	int narr[n+1];
	for (int i = 0; i < n; ++i)
		narr[i]=get_int();
	int t=get_int();
	while(t--)
	{
		int lb=get_int();
		int hb=get_int();
		long long int mod=get_long_int();
		long long int result=1;
		for (int i = lb-1; i < hb ; ++i)
			result*=narr[i]%mod;
		printf("%lld\n", result%mod);
	}
	return 0;
}