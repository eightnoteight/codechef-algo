using namespace std;
#include <iostream>
#include <stdio.h>
inline long long int get_int()
{
	long long int n=0;
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();

	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
int main(int argc, char const *argv[])
{
	long long int n=get_int(), k=get_int();
	long long int count=0;
	for (int i = 0; i < n; ++i)
	{
		long long int t=get_int();
		if (!(t%k))
			count++;
	}
	cout << count;
	return 0;
}