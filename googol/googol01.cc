#include <bits/stdc++.h>
using namespace std;
#define builtin_gcd __gcd
#include <bits/stdc++.h>
using namespace std;
#define builtin_gcd __gcd
long long int powmod(long long int base,long long int exp, long long int m)
{
    if(exp==0)
        return 1;
    if(exp==1)
        return base % m;
    if(exp%2 == 0)
    {
        long long int base1 = pow(powmod(base, exp/2, m), 2);
        if(base1 >= m)
            return base1%m;
        else
            return base1;
    }
    else
    {
        long long int ans = (base*pow(powmod(base, (exp-1)/2, m), 2));
        if(ans >= m)
            return ans%m;
        else
            return ans;
    }
}
void divmod(int64_t a, int64_t b, int64_t& quo, int64_t& rem) {
    quo = a / b;
    rem = a % b;
}
void extended_gcd(int64_t aa, int64_t bb, int64_t& g, int64_t& xx, int64_t& yy) {
    int64_t lastr, rem, x, y, lastx, lasty;
    lastr = abs(aa);
    rem = abs(bb);
    x = 0; lastx = 1, y = 1; lasty = 0;
    while(rem) {
        int64_t tmp, quo, tmp2;
        tmp = lastr;
        lastr = rem;
        divmod(tmp, rem, quo, tmp2);
        rem = tmp2;
        tmp = lastx - quo*x;
        lastx = x;
        x = tmp;
        tmp = lasty - quo*y;
        lasty = y;
        y = tmp;
    }
    g = lastr;
    if (aa < 0)
        xx = -lastx;
    else
        xx = lastx;
    if (bb < 0)
        yy = -lasty;
    else
        yy = lasty;
}
long long int invmod(long long int a, long long int b)
{
    int64_t g, x, y;
    extended_gcd(a, b, g, x, y);
    return x % b;
}
int main(int argc, char const *argv[])
{
    // ios::sync_with_stdio(0);cin.tie(0);
    int t;
    cin >> t;
    long long int m = 215372682525;
    while(t--) {
        long long int a, d, n, x;
        cin >> a >> d >> n >> x;
        long long int F=0;
        F = powmod(x, n + 1, m);
        if (F == 0)
            F = m - 1;
        else
            F--;
        F = (F * a) % m;
        if (x == 0)
            F = (F*invmod(m - 1, m)) % m;
        else
            F = (F*invmod(x - 1, m)) %;
        cout << "F=" << F << '\n';
        long long int G;
        long long int one;
        if (x == 0){
            one = m - 1;
        }
        else{
            one = powmod(x, n, m);
            if (one == 0)
                one = m;
            one--;
        }
        long long int two;
        if (n == 0){
            two = (n + 1) - n*invmod(x, m);
        }
        else{
            two = (((n + 1)*powmod(x, n, m)) % m) - (n*powmod(x, n, m));
        }
        long long int three;
        if (n == 0) {
            if (x)
                three = (invmod(x, m)*((x - 1)*(x - 1))) % m;
            else
                three = invmod(x, m);
        }
        else {
            if (x) {
                three = (powmod(x, n - 1, m)*((x - 1)*(x - 1))) % m;
            }
            else {
                three = powmod(x, n - 1, m);
            }
        }
        long long int four;
        four = (n*powmod(x, n, m)) % m;
        long long int five;
        if (x) {
            five = invmod(x - 1, m);
        }
        else {
            five = invmod(m - 1, m);
        }
        if (((((one*two) % m)*three) % m) >= ((four*five) % m)){
            G = (((((one*two) % m)*three) % m) - ((four*five) % m)) % m;
        }
        else {
            G = (((((((long long int)one*(long long int)two) % m)*(long long int)three) % m) - (((long long int)four*(long long int)five) % m)) % m);
        }
        cout << (F + G) % m << '\n';
    }
    return 0;
}

