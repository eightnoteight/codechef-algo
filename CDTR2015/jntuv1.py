#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,bad-builtin,missing-docstring
'''input
5
'''


def main():
    arr = ['3', '4']
    k = 0
    z = 0
    while len(arr) < 101:
        t = len(arr)
        for x in xrange(k, t):
            arr.append('3' + arr[x])
        for x in xrange(k, t):
            arr.append('4' + arr[x])
        z += 1
        k += 2**z
    #print arr
    print arr[int(raw_input()) - 1]


main()
