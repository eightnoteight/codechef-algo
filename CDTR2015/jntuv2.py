#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,bad-builtin,missing-docstring
'''input
20 30
'''
from sys import stdin


def main():
    print 'Result =', sum(map(int, stdin.read().split()))

main()
