#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,bad-builtin,missing-docstring
'''input
5
3 4 5 6 7
7
'''
from collections import defaultdict
from sys import stdin


def main():
    stdin.readline()
    nums = map(int, stdin.read().split())
    x = nums.pop()
    counts = defaultdict(int)
    for num in nums:
        counts[num] += 1
    numset = set(nums)
    for num in nums:
        if x - num in counts and (x != 2*num or (x == 2*num and counts[num] > 1)):
            print 'YES'
            break
    else:
        print 'NO'

main()
