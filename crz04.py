#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap

def main():
    dstream = imap(int, stdin.read().split())
    for _ in xrange(next(dstream)):
        hh, mm = next(dstream), next(dstream)
        if 0 <= hh < 24 and 0 <= mm < 60:
            print "%.1f" % ((- (hh*30) % 360 + mm*6 + (mm / 2)) % 360)
        else:
            print 'Invalid Time'

main()
