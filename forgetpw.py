for t in xrange(int(raw_input())):
    n = int(raw_input())
    rules = {}
    for r in xrange(n):
        tk, tv = raw_input().split()
        rules[tk] = tv
    s = [rules.get(ch, ch) for ch in raw_input()]
    tmp = ''.join(s)
    tmp = tmp.strip('0')
    tmp = tmp.lstrip('0')
    if tmp[-1] == '.':
        tmp = tmp[:-1]
    if tmp == '':
        tmp = '0'
    print tmp
