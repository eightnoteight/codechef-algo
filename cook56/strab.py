#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
import sys


d = {
    'a': '0',
    'b': '1',
    'c': '2',
    'd': '3',
    'e': '4',
    'f': '5',
    'h': '6',
    'i': '7',
    'j': '8',
    'k': '9',
    'l': 'a',
    'm': 'b',
    'n': 'c',
    'o': 'd',
    'p': 'e',
    'q': 'g',
    'r': 'h',
    's': 'i',
    't': 'j',
    'u': 'k',
    'v': 'l',
    'w': 'm',
    'x': 'n',
    'y': 'o',
    'z': 'p',
}


def get_ans(n, m, ns):
    if n < m:
        return pow(26, n, 1000000007)
    return (26*get_ans(n - 1, m, ns) - ns*get_ans(n - m, m, ns)) % 1000000007
def main():
    dstream = iter(sys.stdin.read().split())
    for _ in xrange(int(next(dstream))):
        n, m = int(next(dstream)), int(next(dstream))
        x, y = next(dstream).lower(), next(dstream).lower()
        xc = []
        for chx in x:
            xc.append(d[chx])
        x = ''.join(xc)
        yc = []
        for chy in y:
            yc.append(d[chy])
        y = ''.join(yc)
        ns = int(y, 26) - int(x, 26) + 1
        print get_ans(n, m, ns)


main()

