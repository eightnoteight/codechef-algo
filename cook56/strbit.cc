#include <bits/stdc++.h>
using namespace std;
void print(const char* format, ...);
template <class number>
void input(number *ptr);

class SegmentTree
{
private:
    int _calc_size(int n) {
        int x = 1;
        while ((1 << x) < n)
            x++;
        return x;
    }
    void _rangeadd(int lo, int hi, int idx, int par){
        if(lo >= hi)
            return;
        tree[idx] += par;
        if (ra_hi <= lo or ra_lo >= hi)
            return;
        if (ra_lo <= lo and hi <= ra_hi) {
            tree[idx] += prop_val;
            return;
        }
        int mid = lo + ((hi - lo) / 2);
        _rangeadd(lo, mid, 2*idx + 1, tree[idx]);
        _rangeadd(mid, hi, 2*idx + 2, tree[idx]);
        tree[idx] = 0;
    }
    void _push_prop(int lo, int hi, int idx, int par) {
        if (lo >= hi)
            return;
        if (hi - lo == 1)  {
            tree[idx] += par;
            arr[lo] = tree[idx];
            tree[idx] = 0;
            return;
        }
        int mid = lo + ((hi - lo) / 2);
        _push_prop(lo, mid, 2*idx + 1, tree[idx] + par);
        _push_prop(mid, hi, 2*idx + 2, tree[idx] + par);
        tree[idx] = 0;
    }
    void _find(int lo, int hi, int idx, int par){
        if(lo >= hi)
            return;
        tree[idx] += par;
        if(hi <= query or lo > query)
            return;
        if(hi - lo == 1){
            arr[lo] += tree[idx];
            tree[idx] = 0;
            return;
        }
        int mid = lo + ((hi - lo) / 2);
        _find(lo, mid, 2*idx + 1, tree[idx]);
        _find(mid, hi, 2*idx + 2, tree[idx]);
        tree[idx] = 0;
    }
    vector<int> tree;
    int ra_lo;
    int prop_val;
    int ra_hi;
    int query;
public:
    vector<int> arr;
    SegmentTree(size_t n) {
        arr.resize(n);
        tree.resize((1 << (_calc_size(n) + 1)) + 1);
    }
    void rangeadd(int a, int b) {
        if(a == b)
            return;
        ra_lo = a;
        ra_hi = b;
        prop_val = 1;
        _rangeadd(0, arr.size(), 0, 0);
    }
    void push_prop() {
        _push_prop(0, arr.size(), 0, 0);
    }
    int find(int q) {
        query = q;
        _find(0, arr.size(), 0, 0);
        return arr[q];
    }
};

int main(int argc, char const *argv[])
{
    ios::sync_with_stdio(0);cin.tie(0);
    int t;
    cin >> t;
    while(t--){
        int n, k;
        cin >> n >> k;
        string s;
        cin >> s;
        SegmentTree segt(n);
        int count = 0;
        for (int i = 0; i < n; ++i)
        {
            int tmp = 0;
            if (s[i] == 'R')
                tmp = 1;
            if(((tmp + segt.find(i)) % 2) == 0)
                continue;
            segt.rangeadd(i + 1, min(n, i + k));
            count += 1;
        }
        cout << count << '\n';
    }
    return 0;
}
