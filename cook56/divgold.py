#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
import sys

def brute(s):
    arr = []
    for x in xrange(len(s)):
        ns = s[:x] + s[x + 1:]
        for y in xrange(len(s)):
            arr.append('{}{}{}'.format(ns[:y], s[x], ns[y:]))
    return min(arr)

def main():
    dstream = iter(sys.stdin.read().split())
    for _ in xrange(int(next(dstream))):
        _, s = next(dstream), next(dstream)
        # start = len(s) - 1
        # for i, (chx, chy) in enumerate(zip(s, sorted(s))):
        #     if chx != chy:
        #         start = i
        #         break
        # pos = start
        # for x in xrange(start, len(s)):
        #     if s[pos] > s[x]:
        #         pos = x
        # print ''.join([s[:start], s[pos], s[start:pos], s[pos + 1: len(s)]])
        print brute(s)

main()
