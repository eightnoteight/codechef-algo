#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from math import log, ceil
import sys

class SegmentTree(object):
    def __init__(self, n):
        self.arr = [0] * n
        self.tree = [0] * (2*(pow(2, int(ceil(log(n, 2))))) - 1)

    def addrange(self, ra_lo, ra_hi, val):
        if ra_hi - ra_lo == 0:
            return
        tree = self.tree
        def _addrange(lo, hi, idx, par):
            if lo >= hi:
                return
            tree[idx] += par
            if ra_hi <= lo or ra_lo >= hi:
                return
            if ra_lo <= lo and hi <= ra_hi:
                tree[idx] += val
                return
            mid = lo + ((hi - lo) // 2)
            _addrange(lo, mid, 2*idx + 1, tree[idx])
            _addrange(mid, hi, 2*idx + 2, tree[idx])
            tree[idx] = 0
            return

        return _addrange(0, len(self.arr), 0, 0)

    def find(self, query):
        tree = self.tree
        arr = self.arr
        def _find(lo, hi, idx, par):
            if lo >= hi:
                return
            tree[idx] += par
            if hi <= query or lo > query:
                return
            if hi - lo == 1:
                arr[lo] += tree[idx]
                tree[idx] = 0
                return
            mid = lo + ((hi - lo) // 2)
            _find(lo, mid, 2*idx + 1, tree[idx])
            _find(mid, hi, 2*idx + 2, tree[idx])
            tree[idx] = 0
            return
        _find(0, len(arr), 0, 0)
        return arr[query]

def main():
    dstream = iter(sys.stdin.read().split())
    for _ in xrange(int(next(dstream))):
        n, k = int(next(dstream)), int(next(dstream))
        s = next(dstream)
        segt = SegmentTree(n)
        count = 0
        for i, chx in enumerate(s):
            if ((chx == 'R') + segt.find(i)) % 2 == 0:
                continue
            segt.addrange(i + 1, min(n, i + k), 1)
            count += 1
            # print i
        print count

main()
