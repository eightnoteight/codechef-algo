using namespace std;
#include <stdio.h>
#include <iostream>
#include <bits/stdc++.h>
inline long long int get_long_int(long long int n=0)
{
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();

	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
int main(int argc, char const *argv[])
{
	long long int t=get_long_int();
	while(t--)
	{
		long long int n=get_long_int();
		printf("%lld\n", (n>>1)+1);
	}
	return 0;
}