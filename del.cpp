#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
int main(int argc, char const *argv[])
{
	vector<int> v={3, 1, 4, 1, 5, 9};
 
    vector<int>::iterator result = min_element(v.begin(), v.end());
    std::cout << "min element at: " << distance(v.begin(), result);
}