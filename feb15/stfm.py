#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
# pep8: disable=E501
from sys import stdin

'''input
5 7
1 2 3 4 5
'''


def main():

    def F(pi):
        factpart = (-1 % m) if pi + 1 >= m else ((facts[pi + 1] - 1) % m)
        factpart = (factpart + (pi*pi*(pi + 1)) // 2) % m
        return factpart % m

    inp = map(int, stdin.read().split())
    m = inp[1]
    if m == 1:
        print 0
        return
    if inp[0] == 1:
        inp[0] = inp[-1]
    else:
        inp[0], inp[1] = inp[-1], inp[-2]
    inp.pop()
    inp.pop()
    facts = [1]*(m + 10)
    for x in xrange(1, m + 10):
        facts[x] = (facts[x - 1] * x) % m

    print sum(map(F, inp)) % m


main()
