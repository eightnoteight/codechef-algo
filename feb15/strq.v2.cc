#include <bits/stdc++.h>
using namespace std;
/*input
*/
char s[1000010];

int main(int argc, char const *argv[])
{
    ios::sync_with_stdio(0);cin.tie(0);
    cin >> s;
    int64_t tests;
    int64_t* ch = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* ce = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* cf = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* hc = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* he = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* hf = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* ec = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* eh = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* ef = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* fc = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* fh = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* fe = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* c = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* h = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* e = (int64_t*)malloc(1000010*sizeof(int64_t));
    int64_t* f = (int64_t*)malloc(1000010*sizeof(int64_t));
    ch[0] = ce[0] = cf[0] = hc[0] = he[0] = hf[0] = ec[0] = eh[0] = ef[0] = \
    fc[0] = fh[0] = fe[0] = c[0] = h[0] = e[0] = f[0] = 0;
    for (int i = 0; s[i]; ++i)
    {
        ch[i + 1] = ch[i];
        ce[i + 1] = ce[i];
        cf[i + 1] = cf[i];
        hc[i + 1] = hc[i];
        he[i + 1] = he[i];
        hf[i + 1] = hf[i];
        ec[i + 1] = ec[i];
        eh[i + 1] = eh[i];
        ef[i + 1] = ef[i];
        fc[i + 1] = fc[i];
        fh[i + 1] = fh[i];
        fe[i + 1] = fe[i];
        c[i + 1] = c[i];
        h[i + 1] = h[i];
        e[i + 1] = e[i];
        f[i + 1] = f[i];
        switch(s[i])
        {
            case 'c':
                c[i + 1]++;
                hc[i + 1] += h[i + 1];
                ec[i + 1] += e[i + 1];
                fc[i + 1] += f[i + 1];
                break;
            case 'h':
                h[i + 1]++;
                ch[i + 1] += c[i + 1];
                eh[i + 1] += e[i + 1];
                fh[i + 1] += f[i + 1];
                break;
            case 'e':
                e[i + 1]++;
                ce[i + 1] += c[i + 1];
                he[i + 1] += h[i + 1];
                fe[i + 1] += f[i + 1];
                break;
            case 'f':
                f[i + 1]++;
                cf[i + 1] += c[i + 1];
                hf[i + 1] += h[i + 1];
                ef[i + 1] += e[i + 1];
                break;
        }
    }
    cin >> tests;
    for (int k = 0; k < tests; ++k)
    {
        char a, b;
        int64_t l, r;
        cin >> a >> b >> l >> r;
        if(a == 'c' and b == 'h')
            cout << (ch[r] - ch[l - 1] - ((h[r] - h[l - 1])*(c[l - 1] - c[0]))) << '\n';
        else if(a == 'c' and b == 'e')
            cout << (ce[r] - ce[l - 1] - ((e[r] - e[l - 1])*(c[l - 1] - c[0]))) << '\n';
        else if(a == 'c' and b == 'f')
            cout << (cf[r] - cf[l - 1] - ((f[r] - f[l - 1])*(c[l - 1] - c[0]))) << '\n';
        else if(a == 'h' and b == 'c')
            cout << (hc[r] - hc[l - 1] - ((c[r] - c[l - 1])*(h[l - 1] - h[0]))) << '\n';
        else if(a == 'h' and b == 'e')
            cout << (he[r] - he[l - 1] - ((e[r] - e[l - 1])*(h[l - 1] - h[0]))) << '\n';
        else if(a == 'h' and b == 'f')
            cout << (hf[r] - hf[l - 1] - ((f[r] - f[l - 1])*(h[l - 1] - h[0]))) << '\n';
        else if(a == 'e' and b == 'c')
            cout << (ec[r] - ec[l - 1] - ((c[r] - c[l - 1])*(e[l - 1] - e[0]))) << '\n';
        else if(a == 'e' and b == 'h')
            cout << (eh[r] - eh[l - 1] - ((h[r] - h[l - 1])*(e[l - 1] - e[0]))) << '\n';
        else if(a == 'e' and b == 'f')
            cout << (ef[r] - ef[l - 1] - ((f[r] - f[l - 1])*(e[l - 1] - e[0]))) << '\n';
        else if(a == 'f' and b == 'c')
            cout << (fc[r] - fc[l - 1] - ((c[r] - c[l - 1])*(f[l - 1] - f[0]))) << '\n';
        else if(a == 'f' and b == 'h')
            cout << (fh[r] - fh[l - 1] - ((h[r] - h[l - 1])*(f[l - 1] - f[0]))) << '\n';
        else if(a == 'f' and b == 'e')
            cout << (fe[r] - fe[l - 1] - ((e[r] - e[l - 1])*(f[l - 1] - f[0]))) << '\n';
    }
return 0;
}
