#include <bits/stdc++.h>
using namespace std;
char s[1000010];

struct node {
    int32_t ch, ce, cf, hc, he, hf, ec, eh, ef, fc, fh, fe, c, h, e, f;
};
node zeronode;
// zeronode.ch = zeronode.ce = zeronode.cf = zeronode.hc = zeronode.he = \
// zeronode.hf = zeronode.ec = zeronode.eh = zeronode.ef = zeronode.fc = \
// zeronode.fh = zeronode.fe = 0;
vector<node> segt(2100000);

template <class T>
bool gint(T& n)
{
    n = 0;
    int8_t sign=1;
    register char c=0;
    while(c<33)
        c=getchar_unlocked();
    if (c=='-')
    {
        sign=-1;
        c=getchar_unlocked();
        if(c == -1)
            return false;
    }
    while(c>='0'&&c<='9')
    {
        n=(n<<3)+(n<<1)+(c-'0');
        c=getchar_unlocked();
        if(c == -1)
        {
            n *= sign;
            return true;
        }
    }
    n *= sign;
return true;
}
int32_t gstr(char* s)
{
    *s = 0;
    char* st = s;
    while(*s < 97)
        *s = getchar_unlocked();
    while(*(s++) > 97)
        *s = getchar_unlocked();
    s--;
    *s = 0;
    return s - st;
}
void gch(char* s)
{
    *s = 0;
    while(*s < 97)
        *s = getchar_unlocked();
}
template<class T>
void pint(T a)
{
    register char c;
    char num[21];
    int8_t i = 0;
    if(a < 0) {
           putchar_unlocked('-');
        a *= -1;
    }
    do
    {
        num[i++] = a%10 + 48;
        a /= 10;
    }  while (a != 0);
    i--;
    while (i >= 0)
        putchar_unlocked(num[i--]);
    putchar_unlocked('\n');
}
inline int8_t getval(char c)
{
    switch(c)
    {
        case 'c':
            return 0;
        case 'h':
            return 1;
        case 'e':
            return 2;
        case 'f':
            return 3;
    }
    return 3;
}
node construct(int32_t lo, int32_t hi, int32_t ind)
{
    node now;
    now.ch = now.ce = now.cf = now.hc = now.he = now.hf = now.ec = \
    now.eh = now.ef = now.fc = now.fh = now.fe = now.c = now.h = now.e = \
    now.f = 0;
    if (hi - lo < 1) {
        now.ch = now.ce = now.cf = now.hc = now.he = now.hf = now.ec = \
        now.eh = now.ef = now.fc = now.fh = now.fe = now.c = now.h = now.e = \
        now.f = 0;
        return now;
    }
    if (hi - lo == 1) {
        switch(s[lo]) {
            case 'c':
                now.c = 1;
                break;
            case 'h':
                now.h = 1;
                break;
            case 'e':
                now.e = 1;
                break;
            case 'f':
                now.f = 1;
                break;
        }
        segt[ind] = now;
        // cout << s[lo] << "   " << lo << "\n";
        return now;
    }

    int32_t mid = lo + ((hi - lo) / 2);
    // cout << "pass\n";
    node left = construct(lo, mid, 2*ind + 1);
    node right = construct(mid, hi, 2*ind + 2);
    now.ch = left.ch + right.ch + (left.c)*(right.h);
    now.ce = left.ce + right.ce + (left.c)*(right.e);
    now.cf = left.cf + right.cf + (left.c)*(right.f);
    now.hc = left.hc + right.hc + (left.h)*(right.c);
    now.he = left.he + right.he + (left.h)*(right.e);
    now.hf = left.hf + right.hf + (left.h)*(right.f);
    now.ec = left.ec + right.ec + (left.e)*(right.c);
    now.eh = left.eh + right.eh + (left.e)*(right.h);
    now.ef = left.ef + right.ef + (left.e)*(right.f);
    now.fc = left.fc + right.fc + (left.f)*(right.c);
    now.fe = left.fe + right.fe + (left.f)*(right.e);
    now.fh = left.fh + right.fh + (left.f)*(right.h);
    now.c = left.c + right.c;
    now.h = left.h + right.h;
    now.e = left.e + right.e;
    now.f = left.f + right.f;
    segt[ind] = now;
    return now;
}
//        0, n, l,  h,  0
node find(int32_t l, int32_t r, int32_t lo, int32_t hi, int32_t ind)
{
    node now;
    now.ch = now.ce = now.cf = now.hc = now.he = now.hf = now.ec = \
    now.eh = now.ef = now.fc = now.fh = now.fe = now.c = now.h = now.e = \
    now.f = 0;
    if((r <= lo) or (hi <= l)){
        return now;
    }
    if((lo <= l) and (r <= hi))
        return segt[ind];
    int32_t mid = l + ((r - l) / 2);
    node left = find(l, mid, lo, hi, 2*ind + 1);
    node right = find(mid, r, lo, hi, 2*ind + 2);
    now.ch = left.ch + right.ch + (left.c)*(right.h);
    now.ce = left.ce + right.ce + (left.c)*(right.e);
    now.cf = left.cf + right.cf + (left.c)*(right.f);
    now.hc = left.hc + right.hc + (left.h)*(right.c);
    now.he = left.he + right.he + (left.h)*(right.e);
    now.hf = left.hf + right.hf + (left.h)*(right.f);
    now.ec = left.ec + right.ec + (left.e)*(right.c);
    now.eh = left.eh + right.eh + (left.e)*(right.h);
    now.ef = left.ef + right.ef + (left.e)*(right.f);
    now.fc = left.fc + right.fc + (left.f)*(right.c);
    now.fe = left.fe + right.fe + (left.f)*(right.e);
    now.fh = left.fh + right.fh + (left.f)*(right.h);
    now.c = left.c + right.c;
    now.h = left.h + right.h;
    now.e = left.e + right.e;
    now.f = left.f + right.f;
return now;
}
int32_t good_strings(char a, char b, int32_t l, int32_t r, int32_t n)
{
    if (r - l <= 1)
        return 0;
    node res = find(0, n, l, r, 0);
    if(a == 'c' and b == 'h')
        return res.ch;
    if(a == 'c' and b == 'e')
        return res.ce;
    if(a == 'c' and b == 'f')
        return res.cf;
    if(a == 'h' and b == 'c')
        return res.hc;
    if(a == 'h' and b == 'e')
        return res.he;
    if(a == 'h' and b == 'f')
        return res.hf;
    if(a == 'e' and b == 'c')
        return res.ec;
    if(a == 'e' and b == 'h')
        return res.eh;
    if(a == 'e' and b == 'f')
        return res.ef;
    if(a == 'f' and b == 'c')
        return res.fc;
    if(a == 'f' and b == 'h')
        return res.fh;
    if(a == 'f' and b == 'e')
        return res.fe;

}
int main(int argc, char const *argv[])
{
    int32_t n = gstr(s);
    // cout << n << '\n' << strlen(s) << '\n';
    int32_t tests;
    construct(0, n, 0);
    // cout << s << endl;
    gint(tests);
    for (int k = 0; k < tests; ++k)
    {
        char a, b;
        int32_t l, r;
        gch(&a); gch(&b); gint(l); gint(r);
        pint(good_strings(a, b, l - 1, r, n));
    }
    return 0;
}
