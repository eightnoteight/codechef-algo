#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
'''input
5
1 1
1 2
1 3
2 3
3 4
'''

def main():
    for _ in xrange(int(raw_input())):
        l, r = map(int, raw_input().split())
        print '%s %s' % solve(l, r)


main()
