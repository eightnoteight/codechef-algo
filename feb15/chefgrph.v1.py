#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
'''input
110 110 1
20 21 50 16
'''
from collections import defaultdict


def main():
    mod = 1000000007
    n, m, k = map(int, raw_input().split())
    ways = [0] * (n + 2)
    ways[-1] = 1
    ways.append(1)
    nw = {}
    route = [defaultdict(list) for _ in xrange(n + 2)]
    for _ in xrange(k):
        nlo, mlo, nhi, mhi = map(int, raw_input().split())
        route[nlo][mlo].append((nhi, mhi))
    for edge0 in xrange(n, -1, -1):
        if edge0:
            ways[edge0] = (m * ways[edge0 + 1]) % mod
        else:
            ways[edge0] = ways[edge0 + 1] % mod
        for edge2 in route[edge0]:
            tmp = 0
            for edge1, edge3 in route[edge0][edge2]:
                tmp = (tmp + ways[edge1 + 1]) % mod
                if (edge1, edge3) in nw:
                    tmp = (tmp + nw[(edge1, edge3)]) % mod
            nw[(edge0, edge2)] = tmp % mod
            ways[edge0] = (ways[edge0] + tmp) % mod

    # print ways[92]
    # print ways[32]
    print ways[0] % mod


main()
