#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
'''input
7
1 1
1 2
1 3
2 3
3 4
15 23
10000000000000009 10000000000000309
'''


def find_path(mat, x, y, n):
    stack = []
    m = 0
    c = 1
    if x + 1 < n and mat[x + 1][y] == 1:
        stack.append((x + 1, y))
    if y - 1 >= 0 and mat[x][y - 1] == 1:
        stack.append((x, y - 1))
    while stack:
        x, y = stack.pop()
        if x > 0 and mat[x - 1][y] == mat[x][y] + 1:
            stack.append((x - 1, y))
        if x < n - 1 and mat[x + 1][y] == mat[x][y] + 1:
            stack.append((x + 1, y))
        if y > 0 and mat[x][y - 1] == mat[x][y] + 1:
            stack.append((x, y - 1))
        if y < n - 1 and mat[x][y + 1] == mat[x][y] + 1:
            stack.append((x, y + 1))
        if m < mat[x][y]:
            m = mat[x][y]
            c = 1
        elif m == mat[x][y]:
            c += 1
    if m == 0:
        return 0, 1
    else:
        return m, (2*c) % 1000000007


def subtask1(l, r):
    mat = [[x ^ y for x in xrange(l, r)] for y in xrange(l, r)]
    # print mat
    ml = 0
    count = 0
    for x in xrange(r - l):
        m, c = find_path(mat, x, x, r - l)
        if m > ml:
            ml = m
            count = c
        elif m == ml:
            count = (count + c) % 1000000007
    return ml, count % 1000000007


def main():
    for _ in xrange(int(raw_input())):
        l, r = map(int, raw_input().split())
        print '%s %s' % subtask1(l, r + 1)


main()
