#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
from sys import stdin


def main():
    inp = iter(map(int, stdin.read().split()))
    h, w, k = next(inp), next(inp), next(inp)
    mat = None

    m_t = 0
    for k_i in xrange(k):
        t = next(inp)
        if m_t < t:
            m_t = t
            mat = [[0]*w for _ in xrange(h)]
            for _ in xrange(t):
                x, y = next(inp), next(inp)
                mat[x - 1][y - 1] = k_i + 1
        else:
            for _ in xrange(t):
                next(inp)
                next(inp)

    for x in xrange(h):
        for y in xrange(w):
            print mat[x][y],
        print


main()

'''input
10 10 9
8
1 2
2 2
3 2
4 2
5 2
6 2
7 2
7 1
11
1 7
1 6
2 5
2 6
5 3
5 1
5 2
2 4
3 4
4 4
5 4
12
4 3
5 3
5 1
5 2
1 2
2 2
2 1
3 1
3 2
3 3
4 1
4 2
8
1 5
2 5
2 4
3 4
3 1
4 1
3 2
3 3
14
2 4
3 4
2 3
3 3
4 3
4 4
1 2
2 2
2 1
3 1
1 3
1 4
1 5
2 5
9
1 1
2 1
2 2
2 3
2 4
2 5
2 6
1 6
1 5
14
5 2
5 3
4 4
5 4
4 5
4 6
1 5
2 5
2 4
3 4
4 2
4 1
3 3
4 3
6
1 2
1 1
1 3
2 3
2 1
2 2
18
1 1
1 2
2 1
3 1
2 2
2 3
2 4
2 5
3 8
3 7
4 7
4 6
2 6
3 6
1 4
1 3
1 6
1 5
'''
