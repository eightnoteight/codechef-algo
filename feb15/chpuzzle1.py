#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
'''input
110 110 1
20 21 50 16
'''
from collections import defaultdict


def cbisect(arr, nval, n):
    # print arr, nval, n
    i, j = 0, len(arr)
    if j - i == 0:
        if nval <= n:
            return n
        return nval
    while j - i > 2:
        mid = (i + j) // 2
        if arr[mid] < nval:
            j = mid
        elif arr[mid] == nval:
            return nval
        else:
            i = mid
    if j - i == 2:
        if nval == arr[i] or nval == arr[i + 1]:
            return nval
        elif nval > arr[i] and nval > arr[i + 1]:
            if nval <= n:
                return n
            return nval
        else:
            return arr[i + 1] if arr[i + 1] >= nval else arr[i]
    if nval > arr[i]:
        if nval <= n:
            return n
        return nval
    return arr[i]


def n_manager(route, n, m, npos, ways, nw, mod):
    ways[n] = m % mod
    count = False
    if npos[0] == n:
        edge0 = npos[0]
        for edge2 in route[edge0]:
            ways[n] = (ways[n] + len(route[edge0][edge2])) % mod
            nw[(edge0, edge2)] = len(route[edge0][edge2]) % mod
        count = True
    return count


def zero_manager(route, n, m, npos, ways, nw, mod):
    t = cbisect(npos, 1, n)
    # print 't here ', t
    ways[0] = (pow(m, t - 1, mod) * ways[t]) % mod
    if npos[-1] == 0:
        edge0 = 0
        edge2 = 0
        for edge1, edge3 in route[edge0][edge2]:
            t = cbisect(npos, edge1 + 1, n)
            ways[0] = (ways[0] + pow(m, t - edge1 - 1, mod) * ways[t]) % mod
            if (edge1, edge3) in nw:
                ways[0] = (ways[0] + nw[(edge1, edge3)]) % mod
            # if it is the main take care about the nw tooo!


def total_ways(route, n, m, npos, mod):
    # print npos
    if not npos:
        return pow(m, n, mod)
    ways = {n + 2: 1, n + 1: 1}
    nw = {}
    st = 0
    en = len(npos)
    if n_manager(route, n, m, npos, ways, nw, mod):
        st = 1
    if npos[-1] == 0:
        en -= 1
    # print ways
    for x in xrange(st, en):
        edge0 = npos[x]
        ways[edge0] = (
            pow(m, cbisect(npos, edge0 + 1, n) - edge0, mod) *
            ways[cbisect(npos, edge0 + 1, n)]) % mod
        for edge2 in route[edge0]:
            tmp = 0
            for edge1, edge3 in route[edge0][edge2]:
                t = cbisect(npos, edge1 + 1, n)
                # print edge0, edge2, edge1, edge3, t, npos
                tmp = (tmp + pow(m, t - edge1 - 1, mod) * ways[t]) % mod
                if (edge1, edge3) in nw:
                    tmp = (tmp + nw[(edge1, edge3)]) % mod
            nw[(edge0, edge2)] = tmp
            ways[edge0] = (ways[edge0] + tmp) % mod
    # print ways
    zero_manager(route, n, m, npos, ways, nw, mod)
    # print ways
    return ways[0] % mod


def main():
    mod = 1000000007
    n, m, k = map(int, raw_input().split())
    route = defaultdict(dict)
    for _ in xrange(k):
        nlo, mlo, nhi, mhi = map(int, raw_input().split())
        try:
            route[nlo][mlo].append((nhi, mhi))
        except KeyError:
            route[nlo][mlo] = [(nhi, mhi)]
    route = dict(route)
    print total_ways(route, n, m, sorted(route.keys(), reverse=True), mod)


main()
