#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
'''input
4
1 1
3 6
3 5
3 3
'''


def main():
    def minop(n, s):
        op = 0
        s = (n*(n + 1)) // 2 - s

        while s:
            if s >= n - 1:
                s -= n - 1
                n -= 1
                op += 1
            else:
                op += 1
                break
        return op

    nn12 = [0]
    for x in xrange(1, 141521):
        nn12.append(nn12[-1] + x)
    # print nn12[-1]

    for _ in xrange(int(raw_input())):
        n, s = map(int, raw_input().split())
        # t = bisect(nn12, (n*(n + 1)) // 2 - s)
        # print t
        print minop(n, s)


main()
