#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
'''input
checfcheff
5
c h 1 10
c f 1 10
e c 1 10
c f 1 5
c f 6 10
'''
from sys import stdin


def main():
    s = raw_input()
    n = len(s)
    arr = {
        'ch': [0]*(n + 1),
        'ce': [0]*(n + 1),
        'cf': [0]*(n + 1),
        'hc': [0]*(n + 1),
        'he': [0]*(n + 1),
        'hf': [0]*(n + 1),
        'ec': [0]*(n + 1),
        'eh': [0]*(n + 1),
        'ef': [0]*(n + 1),
        'fc': [0]*(n + 1),
        'fh': [0]*(n + 1),
        'fe': [0]*(n + 1),
        'c': [0]*(n + 1),
        'h': [0]*(n + 1),
        'e': [0]*(n + 1),
        'f': [0]*(n + 1),
    }
    for i, char in enumerate(s):
        for x in arr:
            arr[x][i + 1] = arr[x][i]
        arr[char][i + 1] += 1
        for x in 'chef':
            if x != char:
                arr[x + char][i + 1] += arr[x][i + 1]
    inp = stdin.read().split()
    out = []
    puts = out.append
    for _ in xrange(int(inp[0])):
        a, b, l, r = (
            inp[4*_ + 1],
            inp[4*_ + 2],
            int(inp[4*_ + 3]),
            int(inp[4*_ + 4]))
        puts(str(
            arr[a + b][r] - arr[a + b][l - 1] -
            (arr[a][l - 1]*(arr[b][r] - arr[b][l - 1]))))

    print '\n'.join(out)


main()
