#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin

def main():
    items = zip(*[iter(stdin.read().split())]*2)
    money_sorted = sorted(items, key=lambda x: eval(x[0]), reverse=True)
    print 'Money:'
    for x in money_sorted:
        print '[{}, {}]'.format(*x)
    date_sorted = sorted(
        items,
        key=lambda x: (
            eval(x[1].split('/')[2]),
            eval(x[1].split('/')[0]),
            eval(x[1].split('/')[1])))
    print 'Date:'
    for x in date_sorted:
        print '[{}, {}]'.format(*x)


main()
