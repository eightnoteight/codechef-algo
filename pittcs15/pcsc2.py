#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

class fibmatwhip(object):
    def __init__(self,  f0=0, f1=1, mat=None):
        self.mat = mat or (1, 1, 1, 0)
        self.cache = {
            0: self.mat,
        }
        self.f0 = f0
        self.f1 = f1
        self.cache_max = 0

    def fib(self, n):
        from math import log
        cm = self.cache_max
        while int(log(n, 2)) > cm:
            a, b, c, d = self.cache[cm]
            self.cache[cm + 1] = (
                a * a + b * b,
                b * (a + d),
                b * (a + d),
                b * b + d * d
            )
            cm += 1
        self.cache_max = cm
        ans = self.cache[int(log(n, 2))]
        n -= 1 << int(log(n, 2))
        while n > 0:
            tmp = self.cache[int(log(n, 2))]
            ans = (
                ans[0] * tmp[0] + ans[1] * tmp[2],
                ans[0] * tmp[1] + ans[1] * tmp[3],
                ans[2] * tmp[0] + ans[3] * tmp[2],
                ans[2] * tmp[1] + ans[3] * tmp[3],
            )
            n -= 1 << int(log(n, 2))
        assert ans[1] == ans[2]
        return ans[1]*self.f0 + ans[0]*self.f1

def main():
    a, b, n = map(int, raw_input().split())
    f = fibmatwhip(a, b)
    if n > 2:
        print f.fib(n - 2)
    else:
        print [a, b][n - 1]

if __name__ == '__main__':
    main()
