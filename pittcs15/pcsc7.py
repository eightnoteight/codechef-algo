#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin


def main():
    def josephous(n, k):
        ans = 1
        for x in range(2, n + 1):
            ans = ((ans + k - 1) % x) + 1
        return ans
    n, k = map(int, stdin.read().split()[:2])
    print josephous(n, k)

main()
