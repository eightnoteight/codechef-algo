#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin

def get_block(board, x, y):
    stx = x // 3
    sty = y // 3
    # print "stx=", stx, ";sty =", sty,
    for i in xrange(stx*3, stx*3 + 3):
        for j in xrange(sty*3, sty*3 + 3):
            yield board[i][j]

def answer(board, x, y, q):
    occ = set(board[x])
    occ = occ.union(board[t][y] for t in xrange(9))
    occ = occ.union(get_block(board, x, y))
    # print occ
    if q in occ:
        return 'NO'
    else:
        return 'YES'

def main():
    dstream = iter(stdin.read().split())
    board = [[eval(next(dstream)) for _ in xrange(9)] for _ in xrange(9)]
    # print board
    for query in zip(dstream, dstream, dstream):
        query = map(int, query)
        print answer(board, query[0], query[1], query[2])

main()
