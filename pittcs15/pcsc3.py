#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin


def main():
    n = int(stdin.read())
    for x in xrange(n):
        print '{0}{1}{0}'.format('-'*x, '*'*(2*(n - x)))
    for x in xrange(n - 1):
        print '{0}{1}{2}{1}{0}'.format('-'*(n - x - 1), '*', '-'*2*x)
    print '*'*2*n


if __name__ == '__main__':
    main()
