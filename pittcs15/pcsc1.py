#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin

def septenary(seven, x):
    s = []
    for p7 in seven:
        s.append(str(x // p7))
        x = x % p7
    return int(''.join(s))


def main():
    dstream = map(int, stdin.read().split())
    dstream.pop()
    seven = [7**x for x in xrange(25, -1, -1)]
    for x in dstream:
        print septenary(seven, x),

if __name__ == '__main__':
    main()
