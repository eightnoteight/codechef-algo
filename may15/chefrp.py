#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    for _ in xrange(int(raw_input())):
        raw_input()
        arr = map(int, raw_input().split())
        print -1 if min(arr) == 1 else (sum(arr) - min(arr) + 2)

main()
