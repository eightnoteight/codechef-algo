#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
def main():
    def devstr(b, n, k):
        cc, flips = 1, 0
        for x in xrange(1, n):
            if b[x] != b[x - 1]:
                cc = 1
                continue
            cc += 1
            if cc == k + 1:
                flips += 1
                if x != n - 1 and b[x + 1] == b[x]:
                    b[x] = '01'[b[x] == '0']
                elif x == n - 1:
                    b[x] = '01'[b[x] == '0']
                else:
                    b[x - 1] = '01'[b[x] == '0']
                cc = 1
        return flips, ''.join(b)
    dstream = iter(stdin.read().split())
    for _ in xrange(int(next(dstream))):
        n, k, b = int(next(dstream)), int(next(dstream)), next(dstream)
        if k == 1:
            oz = b[::2].count('1') + b[1::2].count('0')
            zo = b[::2].count('0') + b[1::2].count('1')
            assert oz == n - zo
            s = []
            if oz > zo:
                for x in xrange(n):
                    s.append('0' if x % 2 else '1')
                print zo
            else:
                for x in xrange(n):
                    s.append('1' if x % 2 else '0')
                print oz
            print ''.join(s)
            continue
        print '%s\n%s' % devstr(list(b), n, k)
main()
