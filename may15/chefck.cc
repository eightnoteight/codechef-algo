#include <bits/stdc++.h>
using namespace std;
#define builtin_gcd __gcd
#define min(X, Y) (((X) < (Y)) ? (X) : (Y))
#define _INF 1073741824

template <class number>
void input(number *ptr){
    register char c = getchar_unlocked();
    bool sign = false;
    while (c < 33)
        c = getchar_unlocked();
    *ptr = 0;
    if (c == '-'){
        sign = true;
        c = getchar_unlocked();
    }
    while (c > 33){
        *ptr = (*ptr * 10) + (c - '0');
        c = getchar_unlocked();
    }
    if (sign)
        *ptr *= -1;
}

void print(const char* format, ...){
    va_list arguments;
    va_start(arguments, format);
    register char c;
    char num[20];
    int64_t i = 0, j;
    long n;
    while (format[i] != '\0'){
        if (format[i] != '{')
            putchar_unlocked(format[i++]);
        else{
            i += 2;
            n = va_arg(arguments, long);
            j = 0;
            do{
                num[j++] = n%10 + 48;
                n /= 10;
            }  while (n != 0);
            j--;
            while (j >= 0)
                putchar_unlocked(num[j--]);
        }
    }
    va_end(arguments);
}

class rangeMinimumQuery {
private:
    int** dp;
    char* logt;  // char: performance hack
    int64_t n;
    vector<int64_t>& arr;
public:
    rangeMinimumQuery(vector<int64_t>& array): arr(array) {
        n = arr.size();
        dp = new int*[n];
        logt = new char[n + 1];
        logt[0] = logt[1] = 0;
        for (int64_t x = 2; x <= n; ++x) {
            logt[x] = logt[x >> 1] + 1;
        }
        for (int64_t x = 0; x < n; ++x) {
            dp[x] = new int[1 + logt[n]];
            dp[x][0] = x;
        }
        for (int64_t x = n - 1; x >= 0 ; --x) {
            for (int64_t y = 1; x + (1 << y) <= n; y++) {
                if (arr[dp[x][y - 1]] < arr[dp[x + (1 << (y - 1))][y - 1]])
                    dp[x][y] = dp[x][y - 1];
                else
                    dp[x][y] = dp[x + (1 << (y - 1))][y - 1];
            }
        }
    }
    inline int64_t query(int64_t x, int64_t y) {
        int64_t k = log2(y - x);
        if (arr[dp[x][k]] < arr[dp[y - (1 << k)][k]])
            return dp[x][k];
        else
            return dp[y - (1 << k)][k];
    }
    ~rangeMinimumQuery() {
        for (int64_t x = 0; x < n; ++x) {
            delete[] dp[x];
        }
        delete[] dp;
        delete[] logt;
    }
};

inline void genseg(vector<int64_t>& arr, vector<int64_t>& segments, int64_t bs) {
    int64_t m = arr[0];
    int64_t n = arr.size();
    for (int64_t i = 1; i < n; ++i) {
        if (i % bs == 0) {
            segments.push_back(m);
            m = arr[i];
        }
        else if (arr[i] < m) {
            m = arr[i];
        }
    }
    segments.push_back(m);
}

int64_t query(vector<int64_t>& arr, rangeMinimumQuery& segrmq, int64_t i, int64_t j, int64_t blocksize) {
    if ((j - i) <= 3*blocksize) {
        int64_t m1 = arr[i];
        for (int64_t x = i; x < j; ++x) {
            if (arr[x] < m1)
                m1 = arr[i];
        }
        return m1;
    }
    int64_t m1, m2, m3;
    int64_t up = i;
    while((up % blocksize) != 0) up++;
    int64_t lo = j;
    while((lo % blocksize) != 0) lo--;
    m1 = segrmq.query(up / blocksize, lo / blocksize);
    m2 = m3 = _INF;
    // cout << "=========" << i << ", " << up <<", " <<m2 << endl;
    for (int64_t x = i; x < up; ++x) {
        if (arr[x] < m2)
            m2 = arr[x];
    }
    // cout << "=========" << i << ", " << up <<", " <<m2 << endl;
    for (int64_t x = lo; x < j; ++x) {
        if (arr[x] < m3)
            m3 = arr[x];
    }
    return min(m1, min(m2, m3));
}

int main() {
    int64_t k, q, a, b, c, d, e, f, r, s, t, m, n;
    int64_t l1, la, lc, lm, d1, da, dc, dm;
    int64_t last, lnew;
    input(&n); input(&k); input(&q);
    vector<int64_t> arr(n);
    input(&a); input(&b); input(&c); input(&d); input(&e); input(&f); input(&r); input(&s); input(&t); input(&m); input((int64_t*)&arr[0]);
    int64_t ptxtmp = t;
    for (int64_t x = 1; x < n; ++x) {
        ptxtmp = (ptxtmp * t) % s;
        if (ptxtmp <= r) {
            arr[x] = (arr[x - 1]*arr[x - 1]) % m;
            arr[x] = (arr[x] * a) % m;
            arr[x] = (arr[x] + ((b*arr[x - 1]) % m)) % m;
            arr[x] = (arr[x] + c) % m;
        }
        else{
            arr[x] = (arr[x - 1]*arr[x - 1]) % m;
            arr[x] = (arr[x] * d) % m;
            arr[x] = (arr[x] + ((e*arr[x - 1]) % m)) % m;
            arr[x] = (arr[x] + f) % m;
        }
        // if(arr[x] < 0)
        //     cout << "report x=" << x << endl;
    }
    // TODO: generate the arr
    vector<int64_t> segments;
    int64_t blocksize = log2(n);
    genseg(arr, segments, blocksize);
    rangeMinimumQuery segrmq(segments);


    input(&l1); input(&la); input(&lc); input(&lm); input(&d1); input(&da); input(&dc); input(&dm);
    int64_t tmp = t;

    int64_t totSum = 0, totProd = 1;
    for (int64_t x = 1; x <= q; ++x) {
        l1 = (la*l1 + lc) % lm;
        d1 = (da*d1 + dc) % dm;
        int64_t L = l1 + 1;
        int64_t R = min((L + k - 1 + d1), ((int64_t)n));
        int64_t i = L - 1;
        int64_t j = R - 1;
        tmp = query(arr, segrmq, i, j + 1, blocksize);
        totSum += tmp;
        totProd = (totProd * tmp) % 1000000007;
    }
    print("{} {}", totSum, totProd);
    return 0;
}

