#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap
from collections import defaultdict

def main():
    dstream = imap(int, stdin.read().split())
    n, _ = next(dstream), next(dstream)
    graph = defaultdict(set)
    for x, y in zip(dstream, dstream):
        graph[x - 1].add(y - 1)
        graph[y - 1].add(x - 1)
    ans = float('-inf')
    for x in xrange(2**n):
        hc, uhc = [], set()
        for i, x in enumerate(reversed(bin(x))):
            if x == '1':
                fl = False
                for c in hc:
                    if i in graph[c]:
                        hc, uhc, fl = [], [], True
                        break
                if fl:
                    break
                hc.append(i)
                uhc.update(graph[i])
        ans = max(ans, len(hc) - len(uhc))
    print ans


main()
