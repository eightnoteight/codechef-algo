#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap

def devhand(n, k, m):
    if k == 1:
        return 0
    imod = lambda x: pow(x, m - 2, m)
    kn, kn1, kn2 = [pow(k, n + x, m) for x in xrange(3)]
    ikm1, ikkm1 = imod(k - 1), imod(k*k - 1)
    skx, sk2x, sk3x = [((kn**x - k**x)*imod(k**x - 1)) % m for x in xrange(1, 4)]
    skmx = ((imod(kn) - imod(k))*imod(imod(k) - 1)) % m
    xskx = ((k*(n*kn1 - (n + 1)*kn + 1)*ikm1*ikm1) - n*kn) % m
    return sum([
        (ikm1*(
            (kn2*ikm1*(kn*skx - sk2x)) - (kn*(n*k*skx - k*xskx)) -
            (2*kn1*ikm1*(kn1*(n - 1) - k*skx)) + (kn1*ikm1*(kn1*skmx - k*(n - 1))) +
            ((kn1*n*(n - 1)*imod(2))) - (ikkm1*(kn1*kn1*k*skx - k*k*k*sk3x)) +
            (ikm1*(kn1*k*skx - k*k*sk2x)) + (2*k*ikkm1*(kn1*kn1*(n - 1) - k*k*sk2x)) -
            (k*ikkm1*(kn1*kn1*skmx - k*k*skx)) - (k*ikm1*(kn1*(n - 1) - k*skx)))*6) % m,
        sum([
            y*(k**x)*(kn**x - 1)*imod(k**x - 1)
            for x, y in zip([1, 2, 3], [2, -3, 1])]) % m,
        (3*ikm1*(
            kn1*(sk2x - 3*skx + 2*(n - 1)) - k*(sk3x - 3*sk2x + 2*skx))) % m,
        (3*(((kn1**2)*(skx + skmx - 2*(n - 1)) - (k**2)*(sk3x + skx - 2*sk2x))*ikkm1 -
            ikm1*(kn1*(skx - (n - 1)) - k*(sk2x - skx)))) % m
    ]) % m

def main():
    dstream = imap(int, stdin.read().split())
    for _ in xrange(next(dstream)):
        print devhand(next(dstream), next(dstream), 1000000007)

main()
