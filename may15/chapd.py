#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap

def main():
    def gcd(a, b):
        while b:
            a, b = b, a % b
        return a
    dstream = imap(int, stdin.read().split())
    for _ in xrange(next(dstream)):
        a, b = next(dstream), next(dstream)
        while gcd(a, b) != 1:
            b //= gcd(a, b)
        if b == 1:
            print 'Yes'
        else:
            print 'No'

main()
