#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin

def main():
    dstream = map(int, stdin.read().split())
    mod = 10**9 + 7
    ii = 1
    out = bytearray()
    for _ in xrange(dstream[0]):
        n = dstream[ii]
        arr = sorted([dstream[ii + i] for i in xrange(1, n + 1)])
        ii += n + 1
        setdiff = 0
        for x in xrange(n):
            setdiff = (setdiff + arr[x]*(pow(2, x, mod) - pow(2, n - x - 1, mod))) % mod
        out.extend("%s\n" % setdiff)
    print out

main()
