#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap
from collections import deque, defaultdict

def main():
    def traverse(graph, src, val, cats):
        queue = deque([src])
        visited = set()
        while queue:
            node = queue.popleft()
            if node in visited:
                continue
            visited.add(node)
            cats[node] = val
            for child in graph[node]:
                queue.append(child)

    dstream = imap(int, stdin.read().split())
    n, m = next(dstream), next(dstream)
    graph = [set() for x in xrange(n)]
    for _ in xrange(m):
        u, v = next(dstream), next(dstream)
        graph[u - 1].add(v - 1)
    cats = [None]*n
    cats[0] = 0
    for i, x in enumerate(graph[0]):
        traverse(graph, x, i + 1, cats)
    counter = defaultdict(int)
    for x in cats:
        if x is not None:
            counter[x] += 1
    counts = counter.values()
    tots = sum(counts)
    p, ux = 0, 0
    for x in counts:
        ux += x
        p += x * (tots - ux)
    print p

main()
