#include <bits/stdc++.h>

using namespace std;
#define min(X, Y) (((X) < (Y)) ? (X) : (Y))
int32_t* arr;
int32_t* logTable;
int32_t** rmq;
int32_t n;

void input(int64_t *ptr){
    char c = getchar_unlocked();
    int sign = 0;
    while (c < 33)
        c = getchar_unlocked();
    *ptr = 0;
    if (c == '-'){
        sign = 1;
        c = getchar_unlocked();
    }
    while (c > 33){
        *ptr = (*ptr * 10) + (c - '0');
        c = getchar_unlocked();
    }
    if (sign)
        *ptr *= -1;
}
void print(const char* format, ...){
    va_list arguments;
    va_start(arguments, format);
    register char c;
    char num[20];
    int i = 0, j;
    int64_t n;
    while (format[i] != '\0'){
        if (format[i] != '{')
            putchar_unlocked(format[i++]);
        else{
            i += 2;
            n = va_arg(arguments, int64_t);
            j = 0;
            do{
                num[j++] = n%10 + 48;
                n /= 10;
            }  while (n != 0);
            j--;
            while (j >= 0)
                putchar_unlocked(num[j--]);
        }
    }
    va_end(arguments);
}

int64_t gcd(int64_t a, int64_t b) {
    int64_t tmp;
    while(b) {
        tmp = b;
        b = a % b;
        a = b;
    }
    return a;
}

int64_t powmod(int64_t n, int64_t m, int64_t mod) {
    if (m == 0) {
        return 1;
    }
    if (m == 1) {
        return n;
    }
    if (m % 2) {
        int64_t result = powmod(n, m / 2, mod);
        return (((result*result) % mod)*n) % mod;
    }
    else {
        int64_t result = powmod(n, m / 2, mod);
        return (result*result) % mod;
    }
}


int main() {
    int64_t K, q, a, b, c, d, e, f, r, s, t, m;
    int64_t l1, la, lc, lm, d1, da, dc, dm;
    int64_t last, lnew;
    int32_t i, k, x;
    input((int64_t*)&n); input(&K); input(&q);
    arr = (int32_t*)malloc(sizeof(int32_t)*n);
    input(&a); input(&b); input(&c); input(&d); input(&e); input(&f); input(&r); input(&s); input(&t); input(&m); input((int64_t*)&arr[0]);
    input(&l1); input(&la); input(&lc); input(&lm); input(&d1); input(&da); input(&dc); input(&dm);
    // if (gcd(la, lm) == 1 && n > 5000000)
    //     return 0;
    logTable = (int32_t*)malloc(sizeof(int32_t)*(n + 1));
    logTable[0] = logTable[1] = 0;
    for (i = 2; i <= n; ++i) {
        logTable[i] = logTable[i >> 1] + 1;
    }
    rmq = (int32_t**)malloc(sizeof(int32_t*)*(logTable[n] + 1));
    for (i = 0; i < logTable[n] + 1; ++i) {
        rmq[i] = (int32_t*)malloc(sizeof(int32_t)*n);
    }
    for (i = 0; i < n; ++i) {
        rmq[0][i] = i;
    }
    int64_t tmp = t;
    for (x = 2; x <= n; ++x) {
        tmp = (tmp*t) % s;
        last = arr[x - 2];
        if(tmp <= r){
            lnew = (a*last*last + b*last + c) % m;
        }
        else {
            lnew = (d*last*last + e*last + f) % m;
        }
        arr[x - 1] = lnew;
    }

    for (k = 1; (1 << k) < n; ++k) {
        for (i = 0; i + (1 << k) <= n; ++i) {
            int32_t x = rmq[k - 1][i];
            int32_t y = rmq[k - 1][i + (1 << k - 1)];
            rmq[k][i] = arr[x] <= arr[y] ? x : y;
        }
    }
    int32_t* l1d = (int32_t*)malloc(sizeof(int32_t)*q);
    int32_t* d1d = (int32_t*)malloc(sizeof(int32_t)*q);
    int32_t pln = 0;

    int64_t totSum = 0, totProd = 1;
    for (x = 1; x <= q; ++x) {
        l1 = (la*l1 + lc) % lm;
        d1 = (da*d1 + dc) % dm;
        l1d[x - 1] = l1;
        d1d[x - 1] = d1;
        if (l1d[x - 1] == l1d[0] && d1d[x - 1] == d1d[0] && x != 1) {
            break;
        }
        pln++;
    }
    int32_t qrem = q % pln;
    int64_t tsrem = 0, tprem = 1;
    for (x = 1; x <= pln; ++x) {
        int L = l1d[x - 1] + 1;
        int R = min((L + K - 1 + d1d[x - 1]), ((int64_t)n));
        int i = L - 1;
        int j = R - 1;
        tmp = min(arr[rmq[logTable[j - i]][i]], arr[rmq[logTable[j - i]][j - (1 << logTable[j - i]) + 1]]);
        totSum += tmp;
        totProd = (totProd * tmp) % 1000000007;
        if (x <= qrem) {
            tsrem = totSum;
            tprem = totProd;
        }
    }
    print("{} {}", (totSum * ((int32_t)(q / pln))) + tsrem, (powmod(totProd, ((int32_t)(q / pln)), 1000000007)*tprem) % 1000000007);
    return 0;
}

