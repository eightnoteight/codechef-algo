#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap, izip, cycle


def main():
    def gentab(arr, n):
        logt = [0]*(n + 1)
        for x in xrange(2, n + 1):
            logt[x] = logt[x >> 1] + 1
        rmq = [range(n) for _ in xrange(logt[n] + 1)]
        for k in xrange(1, logt[n] + 1):
            for i in xrange(n - (1 << k) + 1):
                x, y = rmq[k - 1][i], rmq[k - 1][i + (1 << k - 1)]
                rmq[k][i] = x if arr[x] <= arr[y] else y
        return logt, rmq
    def query(i, j):
        return min(arr[rmq[logt[j - i]][i]], arr[rmq[logt[j - i]][j - (1 << logt[j - i]) + 1]])

    dstream = imap(int, stdin.read().split())
    n, k, q = next(dstream), next(dstream), next(dstream)
    arr = [0]*n
    a, b, c, d, e, f, r, s, t, m, arr[0] = [next(dstream) for _ in xrange(11)]
    l1, la, lc, lm, d1, da, dc, dm = [next(dstream) for _ in xrange(8)]
    tmp = t
    for x in xrange(1, n):
        tmp = (tmp * t) % s
        if tmp <= r:
            arr[x] = (a*arr[x - 1]*arr[x - 1] + b*arr[x - 1] + c) % m
        else:
            arr[x] = (d*arr[x - 1]*arr[x - 1] + e*arr[x - 1] + f) % m
    logt, rmq = gentab(arr, n)
    s, p = 0, 1
    l1d, d1d = [], []
    for x in xrange(q):
        l1 = (la*l1 + lc) % lm
        d1 = (da*d1 + dc) % dm
        l1d.append(l1)
        d1d.append(d1)
        if l1d[-1] == l1d[0] and d1d[-1] == d1d[0] and x:
            l1d.pop()
            d1d.pop()
            break
    pln = len(l1d)
    qrem = q % pln
    srem, prem = 0, 1
    for cnt, (l1, d1) in enumerate(izip(l1d, d1d)):
        l = l1 + 1
        r = min(l + k - 1 + d1, n)
        result = query(l - 1, r - 1)
        # print cnt, (l1, d1), l, r, result
        s += result
        p = (p * result) % 1000000007
        if cnt < qrem:
            srem, prem = s, p
    # print q, pln
    print s * (q // pln) + srem, (pow(p, q // pln, 1000000007)*prem) % 1000000007

main()
