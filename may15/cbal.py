#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from collections import defaultdict
from sys import stdin

def main():
    dstream = iter(stdin.read().split())
    for _ in xrange(int(next(dstream))):
        arr = [ord(x) - 97 for x in next(dstream)]
        n = len(arr)
        xorarr = [0]
        for x in arr:
            xorarr.append(xorarr[-1]^(1 << x))
        ka, kb = 0, 0
        for _ in xrange(int(next(dstream))):
            x, y, ty = map(int, [next(dstream), next(dstream), next(dstream)])
            l, r = (x + ka) % n + 1, (y + kb) % n + 1
            if l > r:
                l, r = r, l
            positions = defaultdict(list)
            for x in xrange(l - 1, r + 1):
                positions[xorarr[x]].append(x)
            ans = 0
            for _, pos in positions.iteritems():
                for x in xrange(len(pos)):
                    for y in xrange(x + 1, len(pos)):
                        ans += (pos[y] - pos[x])**ty
            print ans
            ka, kb = kb, ans



main()
