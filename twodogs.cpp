using namespace std;
#include <iostream>
#include <stdio.h>
#include <cstring>
#include <cctype>
#include <cstdlib>
#include <climits>
#include <algorithm>
#include <vector>
#include <set>
#include <bitset>
#include <iterator>
#include <map>
inline int get_int(int n=0)
{
	
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();

	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
int main(int argc, char const *argv[])
{
	int n=get_int(), k=get_int();
	int tint[n];
	int pam[1000002]={0};
	int  wait;
	int minwait= INT_MAX;
	for (int i = 0; i < n; ++i)
	{
		tint[i]=get_int();
		if (tint[i]>=k || (tint[i]==k/2 && (k-tint[i]) == k/2))
			continue;
		pam[tint[i]]=pam[tint[i]]?min(pam[tint[i]],min(i+1,n-i)):min(i+1,n-i);
		if (pam[k-tint[i]])
		{
			wait 		= max(pam[tint[i]],pam[k-tint[i]]);
			minwait 	= min(minwait,wait);
		}
	}
	cout << ((minwait==INT_MAX)?(-1):minwait) << endl;
	return 0;
}