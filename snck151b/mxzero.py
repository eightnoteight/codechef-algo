#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap

def mxzero(arr):
    xor = 0
    for x in arr:
        xor ^= x
    count = 0
    for x in arr:
        if x == xor:
            count += 1
    return count

def main():
    dstream = imap(int, stdin.read().split())
    for _ in xrange(next(dstream)):
        print mxzero([next(dstream) for _ in xrange(next(dstream))])

main()
