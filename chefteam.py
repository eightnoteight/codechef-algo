
def nCr_deprecated(n, r):
    '''
        Implementaion : Dynamic Programming,
                        Observing Pascal Triangle
           Complexity : O(n*r)
     Space Complexity : O(r)

     Pascal Triangle
     ---------------
     1    0
     1    1    0
     1    2    1    0
     1    3    3    1    0
     1    4    6    4    1    0
     1    5   10   10    5    1
    '''
    if n < r:
        return 0
    if r < 2 and n < 2:
        return 1

    pt_row = [0]*(r + 1)
    pt_row[0] = 1

    for x in xrange(n + 1):
        y = min(x, r)
        while y > 0:
            pt_row[y] = pt_row[y] + pt_row[y - 1]
            y -= 1
            # wasting n - r ops here during the last row
            # of the pascal triangle

    return pt_row[r]


def nCr(n, r):
    ans = 1
    for x in xrange(1, r + 1):
        ans *= n - x + 1
    for x in xrange(1, r + 1):
        ans //= x
    return ans


for x in xrange(int(raw_input())):
    n, r = map(int, raw_input().split())
    print(nCr(n, r))
