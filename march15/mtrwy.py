#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from collections import defaultdict
import sys
sys.stdin = open('/tmp/spojtest.in', 'r')
# range = xrange


def connected(wall, p1, p2, n, m):
    visited = [[True]*m for _ in range(n)]
    stack = [p1]
    visited[p1[0]][p1[1]] = False
    if p1 == p2:
        return True
    while stack:
        x, y = stack.pop()
        if x > 0 and visited[x - 1][y] and wall[x - 1][y] < 2:
            if (x - 1, y) == p2:
                return True
            stack.append((x - 1, y))
            visited[x - 1][y] = False
        if y > 0 and visited[x][y - 1] and wall[x][y] % 2 == 0:
            if (x, y - 1) == p2:
                return True
            stack.append((x, y - 1))
            visited[x][y - 1] = False
        if x < n - 1 and visited[x + 1][y] and wall[x][y] < 2:
            if (x + 1, y) == p2:
                return True
            stack.append((x + 1, y))
            visited[x + 1][y] = False
        if y < m - 1 and visited[x][y + 1] and wall[x][y] % 2 == 0:
            if (x, y - 1) == p2:
                return True
            stack.append((x, y + 1))
            visited[x][y + 1] = False
    return False


def traverse(visited, wall, n, m, p):
    count = 1
    stack = [p]
    while stack:
        x, y = stack.pop()
        if x > 0 and visited[x - 1][y] and wall[x - 1][y] < 2:
            stack.append((x - 1, y))
            count += 1
            visited[x - 1][y] = False
        if y > 0 and visited[x][y - 1] and wall[x][y] % 2 == 0:
            stack.append((x, y - 1))
            count += 1
            visited[x][y - 1] = False
        if x < n - 1 and visited[x + 1][y] and wall[x][y] < 2:
            stack.append((x + 1, y))
            count += 1
            visited[x + 1][y] = False
        if y < m - 1 and visited[x][y + 1] and wall[x][y] % 2 == 0:
            stack.append((x, y + 1))
            count += 1
            visited[x][y + 1] = False
    #print(count)
    return count



def component(wall, n, m):
    visited = [[True]*m for _ in range(n)]
    result = float('-inf')
    for x in range(n):
        for y in range(m):
            if visited[x][y]:
                visited[x][y] = False
                tmp = traverse(visited, wall, n, m, (x, y))
                if tmp > result:
                    result = tmp
    return result


def main():
    dstream = iter(map(int, sys.stdin.read().split()))
    for _ in range(next(dstream)):
        n, m, q = next(dstream), next(dstream), next(dstream)
        wall = [[0]*m for _ in range(n)]
        result = 0
        for _ in range(q):
            t = next(dstream)
            if t == 1:
                x, y = next(dstream) - 1, next(dstream) - 1
                wall[x][y] += wall[x][y] % 2 == 0
            elif t == 2:
                x, y = next(dstream) - 1, next(dstream) - 1
                wall[x][y] += 2*(wall[x][y] < 2)
            elif t == 3:
                x1, y1 = next(dstream) - 1, next(dstream) - 1
                x2, y2 = next(dstream) - 1, next(dstream) - 1
                result += connected(wall, (x1, y1), (x2, y2), n, m)
            else:
                result += component(wall, n, m)
        print(result)


main()
