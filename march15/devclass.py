#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
import sys
sys.stdin = open('/tmp/spojtest.in', 'r')

def ans0(item):
    bc = item.count('B')
    gc = item.count('G')
    if abs(bc - gc) > 1:
        return -1
    first, second = 'B', 'G'
    if gc > bc:
        first, second = 'G', 'B'
    elif gc == bc:
        return min(
            (item[::2].count('B') + item[1::2].count('G')) // 2,
            (item[::2].count('G') + item[1::2].count('B')) // 2)
    return (item[::2].count(second) + item[1::2].count(first)) // 2


def ans1(item):
    bc = item.count('B')
    gc = item.count('G')
    if abs(bc - gc) > 1:
        return -1
    first, second = 'B', 'G'
    if gc > bc:
        first, second = 'G', 'B'
    elif gc == bc:
        return min(
            sum(
                [abs(y - x) for x, y in zip(
                    [2*i for i, z in enumerate(item[::2]) if z == 'G'],
                    [2*i + 1 for i, z in enumerate(item[1::2]) if z == 'B'])]),
            sum(
                [abs(y - x) for x, y in zip(
                    [2*i for i, z in enumerate(item[::2]) if z == 'B'],
                    [2*i + 1 for i, z in enumerate(item[1::2]) if z == 'G'])]))
    so = [2*i for i, x in enumerate(item[::2]) if x == second]
    fo = [2*i + 1 for i, x in enumerate(item[1::2]) if x == first]
    return sum([abs(y - x) for x, y in zip(so, fo)])


def main():
    dstream = iter(sys.stdin.read().split())
    funcs = [ans0, ans1, ans1]
    for _ in range(int(next(dstream))):
        print(funcs[int(next(dstream))](next(dstream)))


main()
