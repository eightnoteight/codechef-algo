#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
import sys
#sys.stdin = open('/tmp/spojtest.in', 'r')


def main():
    def answer(s, c, k):
        if c == 0:
            if k <= s:
                return 2**(s - k + 1) + 1
            else:
                return 0
        if s == 0:
            if k == 1:
                return 2**(c + 1) - 2
            else:
                return 0
        sli = list(range(s, 0, -1))
        sli.extend([0]*100)
        cli = [1]*(c)
        cli.extend([0]*100)
        final = [s]
        for x in range(1, max(s, c + 1)):
            final.append(sli[x] + cli[x - 1])
        final.append(0)
        i = 0
        result = 0
        while final[i] >= k:
            result = 2**(i + 1) + 1
            i += 1
        return result

    dstream = iter(map(int, sys.stdin.read().split()))
    for _ in range(next(dstream)):
        print(answer(next(dstream), next(dstream), next(dstream)))


main()
