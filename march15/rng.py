#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
import sys
import numpy as np


def main():
    dstream = iter(map(int, sys.stdin.read().split()))
    k, n = next(dstream), next(dstream)
    Amat = np.mat(list(reversed([[next(dstream)] for _ in range(k)])))
    matrix = [[next(dstream) for _ in range(k)]]
    if n <= k:
        print(Amat.tolist()[k - n][0])
        return
    for x in range(k - 1):
        matrix.append([0]*k)
        matrix[-1][x] = 1
    matrix = np.mat(matrix)
    matpow = [matrix**0, matrix]
    for x in range(70):
        matpow.append((matpow[-1]*matpow[-1]) % 104857601)
    result = matpow[0]
    for i, x in enumerate(reversed(bin(n - k)[2:])):
        if x == '1':
            result = (result * matpow[i + 1]) % 104857601
    result = (result*Amat) % 104857601
    print(result.tolist()[0][0])



main()
