#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
import sys
sys.stdin = open('/tmp/spojtest.in', 'r')

p32 = 4294967296


def gen1(n, s):
    def nInteger(s):
        s = (s * 1103515245 + 12345) % p32
        return ((s // 65536) % 32768), s
    a = []
    for _ in range(n):
        tmp, s = nInteger(s)
        a.append(str(tmp % 2))
    return ''.join(a)

def gen2(n, s):
    def nInteger(x, y, z, w):
        t = (x ^ ((x << 11) % p32)) % p32
        return y, z, w, (((w ^ ((w >> 19) % p32)) % p32) ^ ((t ^ ((t >> 8) % p32)) % p32)) % p32
    a = []
    x, y, z, w = s % p32, (s*s) % p32, (s*s*s) % p32, (s*s*s*s) % p32
    for _ in range(n):
        x, y, z, w = nInteger(x, y, z, w)
        a.append(str(w % 2))
    return ''.join(a)


def testing():
    lcg = gen1(400004, 0)
    dstream = iter(sys.stdin.readlines())
    out = []
    puts = out.append
    for _ in range(int(next(dstream))):
        if next(dstream)[:50] in lcg:
            puts('LCG')
        else:
            puts('Xorshift')
    print('\n'.join(out))

def main():
    threshold = 50
    lcg = []
    for s in range(131073):
        lcg.append(gen1(threshold, s))
    xshift = []
    # for s in range(131073):
        # xshift.append(gen2(threshold, s))
    # print(len(set(lcg).union(xshift)) == len(set(lcg)) + len(set(xshift)))
    lcg = set(lcg)
    #xshift = set(xshift)
    dstream = iter(sys.stdin.readlines())
    out = []
    puts = out.append
    for _ in range(int(next(dstream))):
        if next(dstream)[:50] in lcg:
            puts('LCG')
        else:
            puts('Xorshift')
    print('\n'.join(out))

# main()
testing()
