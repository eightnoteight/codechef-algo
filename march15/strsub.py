#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from bisect import bisect_right, bisect_left
import sys
sys.stdin = open('/tmp/spojtest.in', 'r')
# range = xrange


def get_C0(S):
    c0 = [int(S[0] == 0)]
    for x in range(1, len(S)):
        c0.append(c0[-1])
        if S[x] == 0:
            c0[-1] += 1
    #print(c0)
    return c0



def get_C1(S):
    c1 = [int(S[0] == 1)]
    for x in range(1, len(S)):
        c1.append(c1[-1])
        if S[x] == 1:
            c1[-1] += 1
    return c1


def get_R(S, c0, c1, k):
    mc0 = []
    mc1 = []
    rr = []
    for x in range(len(c0)):
        mc0.append(bisect_right(c0, c0[x] + k - (S[x] == 0)))
        mc1.append(bisect_right(c1, c1[x] + k - (S[x] == 1)))
        rr.append(min(
            bisect_right(c0, c0[x] + k - (S[x] == 0)),
            bisect_right(c1, c1[x] + k - (S[x] == 1))))
    return rr


def sigmaR(srr, rr, i, j):
    pos = bisect_left(rr, j)
    if pos >= i:
        return (j - pos) * j + srr[pos] - rr[pos] - srr[i] + rr[i] - ((j**2 - i**2 - j + i) // 2)
    else:
        return (j - i) * j - ((j**2 - i**2 - j + i) // 2)


def main():
    dstream = iter(sys.stdin.read().split())
    for _ in range(int(next(dstream))):
        _, k, q = map(int, [next(dstream), next(dstream), next(dstream)])
        S = list(map(int, next(dstream)))
        c0 = get_C0(S)
        c1 = get_C1(S)
        rr = get_R(S, c0, c1, k)
        srr = [rr[0]]
        # print(rr)
        for x in range(1, len(rr)):
            srr.append(srr[-1] + rr[x])
        for _ in range(q):
            i, j = int(next(dstream)) - 1, int(next(dstream))
            print(sigmaR(srr, rr, i, j))


main()
