#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from bisect import bisect_left
import sys
#sys.stdin = open('/tmp/spojtest.in', 'r')


def main():
    dstream = iter(map(int, sys.stdin.read().split()))
    n, m, k = next(dstream), next(dstream), next(dstream)
    numbers = []
    locations = [[] for _ in range(m)]
    for i in range(n):
        ai = next(dstream)
        numbers.append(ai)
        locations[ai - 1].append(i)
    for i in range(m):
        locations[i].append(n)
        #print(locations[i])
    for _ in range(k):
        l, r = next(dstream) - 1, next(dstream)
        answer = float('-inf')
        for x in range(l, r):
            if answer >= r - x - 1:
                break
            tmp = locations[numbers[x] - 1][bisect_left(
                locations[numbers[x] - 1], r) - 1] - x
            if tmp > answer:
                answer = tmp
        print(answer)


main()
