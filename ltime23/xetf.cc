#include <bits/stdc++.h>
using namespace std;
int64_t mod = 1000000007;
class InputOutputHandler {
public:
    InputOutputHandler() {
    }
    int16_t operator>>(int16_t ger) {
        char c = getchar_unlocked();
        bool sign = false;
        while (c < 33)
            c = getchar_unlocked();
        ger = 0;
        if (c == '-'){
            sign = true;
            c = getchar_unlocked();
        }
        while (c > 33){
            ger = (ger * 10) + (c - '0');
            c = getchar_unlocked();
        }
        if (sign)
            ger *= -1;
        return ger;
    }
    int32_t operator>>(int32_t ger) {
        char c = getchar_unlocked();
        bool sign = false;
        while (c < 33)
            c = getchar_unlocked();
        ger = 0;
        if (c == '-'){
            sign = true;
            c = getchar_unlocked();
        }
        while (c > 33){
            ger = (ger * 10) + (c - '0');
            c = getchar_unlocked();
        }
        if (sign)
            ger *= -1;
        return ger;
    }
    int64_t operator>>(int64_t ger) {
        char c = getchar_unlocked();
        bool sign = false;
        while (c < 33)
            c = getchar_unlocked();
        ger = 0;
        if (c == '-'){
            sign = true;
            c = getchar_unlocked();
        }
        while (c > 33){
            ger = (ger * 10) + (c - '0');
            c = getchar_unlocked();
        }
        if (sign)
            ger *= -1;
        return ger;
    }
    char operator>>(char c) {
        c = getchar_unlocked();
        return c == -1 ? 0 : c;
    }
    int32_t operator>>(char* data) {
        char* p = data;
        while((*p = getchar_unlocked()) <= ' ');
        while((*p = getchar_unlocked()) > ' ');
        *p = '\0';
        return p - data;
    }
    void operator<<(int16_t n){
        char num[25];
        int16_t j = 0;
        do{
            num[j++] = n%10 + 48;
            n /= 10;
        }  while (n != 0);
        j--;
        while(j >= 0)
            putchar_unlocked(num[j--]);
    }
    void operator<<(int32_t n){
        char num[25];
        int16_t j = 0;
        do{
            num[j++] = n%10 + 48;
            n /= 10;
        }  while (n != 0);
        j--;
        while(j >= 0)
            putchar_unlocked(num[j--]);
    }
    void operator<<(int64_t n){
        char num[25];
        int16_t j = 0;
        do{
            num[j++] = n%10 + 48;
            n /= 10;
        }  while (n != 0);
        j--;
        while(j >= 0)
            putchar_unlocked(num[j--]);
    }
    void operator<<(char c){
        putchar_unlocked(c);
    }
    void operator<<(char* s) {
        while(*s)
            putchar_unlocked(*(s++));
    }
};

int32_t* sieve(int32_t n) {
    int32_t* arr = new int32_t[n];
    for (int32_t i = 0; i < n; ++i)
        arr[i] = i;
    for (int32_t x = 4; x < n; x += 2)
        arr[x] = 0;
    for (int32_t x = 6; x < n; x += 6)
        arr[x] = 0;
    arr[1] = 0;
    int32_t hi = ((int32_t)ceil((sqrt(n) + 1) / 6.0)) + 1;
    for (int32_t x = 0; x < hi; ++x)
    {
        if(arr[6*x - 1])
            for (int32_t y = (6 * x - 1) * (6 * x - 1); y < n; y += 2 * (6 * x - 1))
                arr[y] = 0;
        if(arr[6*x - 1])
            for (int32_t y = (6 * x + 1) * (6 * x + 1); y < n; y += 2 * (6 * x + 1))
                arr[y] = 0;
    }
    int32_t j = 0;
    for (int i = 0; i < n; ++i) {
        if(arr[i]) {
            swap(arr[i], arr[j]);
            j++;
        }
    }
    return arr;
}
long long int powmod(long long int base,long long int exp, long long int m)
{
    if(exp==0)
        return 1;
    if(exp==1)
        return base % m;
    if(exp%2 == 0)
    {
        long long int base1 = pow(powmod(base, exp/2, m), 2);
        if(base1 >= m)
            return base1%m;
        else
            return base1;
    }
    else
    {
        long long int ans = (base*pow(powmod(base, (exp-1)/2, m), 2));
        if(ans >= m)
            return ans%m;
        else
            return ans;
    }
}
int32_t* primes;
int64_t xetf(int32_t n, int32_t k) {
    int64_t ans = 0;
    int32_t p = n;
    for (int i = 0; i < p && primes[i] != 0; ++i)
    {
        int32_t x = primes[i];
        int32_t s = 0;
        while(n % x == 0) {
            n /= x;
            s++;
        }
        if(s)
            ans = (ans + ((powmod(x, s - 1, mod)*(x - 1)) % mod)) % mod;
    }
    return ans;
}

int main(int argc, char** argv) {
    // ios::sync_with_stdio(0);cin.tie(0);
    InputOutputHandler fio();
    int32_t testcases;
    cin >> testcases;
    primes = sieve(1000001);
    while(testcases--) {
        int32_t n, k;
        cin >> n >> k;
        if (k != 0)
            cout << 0 << '\n';
        else
            cout << xetf(n, k) << '\n';
    }
}
