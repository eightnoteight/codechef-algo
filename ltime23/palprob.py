#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin,star-args
from sys import stdin
from itertools import combinations

def main():
    def palindromeness(s):
        x, y = 0, len(s)
        rs = 0
        while y - x > 0 and s[x: x + ((y - x) // 2)] == s[y - 1: y - ((y - x) // 2) - 1: -1]:
            rs += 1
            y = x + ((y - x) // 2)
        # print s, rs
        return rs

    def total(s):
        rs = 0
        # print s
        # print list(combinations(xrange(len(s) + 1), 2))
        for x, y in combinations(xrange(len(s) + 1), 2):
            rs += palindromeness(s[x: y])
        return rs
    dstream = iter(stdin.read().split())
    for _ in xrange(int(next(dstream))):
        print total(next(dstream))

main()
