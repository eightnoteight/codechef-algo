#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin,star-args
from sys import stdin

def main():
    dstream = iter(stdin.read().split())
    for _ in xrange(int(next(dstream))):
        s = next(dstream)
        if len(set(s[::2])) == len(set(s[1::2])) == 1 and len(set(s)) == 2:
            print 'YES'
        else:
            print 'NO'

main()
