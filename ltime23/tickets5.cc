#include <bits/stdc++.h>
using namespace std;
#define builtin_gcd __gcd

int main(int argc, char const *argv[])
{
    ios::sync_with_stdio(0);cin.tie(0);
    int32_t testcases;
    cin >> testcases;
    while(testcases--){
        string s;
        cin >> s;
        set<char> all;
        set<char> odd;
        set<char> even;
        for (int i = 0, c = s[0]; i < s.size(); i++, c = s[i])
        {
            if(i % 2 == 0) {
                even.insert(c);
                if(even.size() > 1)
                    break;
            }
            else {
                odd.insert(c);
                if (odd.size() > 1)
                    break;
            }
            all.insert(c);
        }
        if (all.size() == 2 and even.size() == 1 and odd.size() == 1 and s[0] != s[1]) {
            cout << "YES\n";
        }
        else {
            cout << "NO\n";
        }
    }
    return 0;
}
