#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin,star-args
from math import ceil, sqrt
from sys import stdin

def main():
    mod = 10**9 + 7
    def sieve(n):
        arr = [x for x in xrange(n)]
        for x in xrange(4, n, 2):
            arr[x] = 0
        for x in xrange(9, n, 6):
            arr[x] = 0
        arr[1] = 0
        for x in xrange(1, int(ceil((sqrt(n) + 1) / 6.0)) + 1):
            if arr[6*x - 1]:
                arr[(6 * x - 1) * (6 * x - 1): n: 2 * (6 * x - 1)] = \
                    [0]*int(ceil((n - (6 * x - 1)*(6 * x - 1)) \
                                 / float(2 * (6 * x - 1))))
            if arr[6*x + 1]:
                arr[(6 * x + 1) * (6 * x + 1): n: 2 * (6 * x + 1)] = \
                    [0]*int(ceil((n - (6 * x + 1)*(6 * x + 1)) \
                                 / float(2 * (6 * x + 1))))
        return filter(None, arr)

    primes = sieve(1000001)

    def xetf(n, k=0):
        if k != 0:
            return 0
        ans = 0
        for x in primes:
            s = 0
            while n % x == 0:
                n //= x
                s += 1
            if s:
                ans = (ans + pow(x, s - 1, mod)*(x - 1)) % mod
        return ans

    dstream = iter(map(int, stdin.read().split()))
    for _ in xrange(next(dstream)):
        print xetf(next(dstream), next(dstream))

main()
