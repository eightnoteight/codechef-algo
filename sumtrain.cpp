#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <iterator>

long long int max_path(int** tree,int i,int j,int n,int** memoization)
{
	if (n==0)
		return 0;
	if (*(*(memoization+i)+j))
	{
		return *(*(memoization+i)+j) + *(*(tree+i)+j);
	}
	*(*(memoization+i)+j) = std::max(max_path(tree,i+1,j,n-1,memoization), max_path(tree,i+1,j+1,n-1,memoization));
	return *(*(memoization+i)+j) + *(*(tree+i)+j);
}
int main(int argc, char const *argv[])
{
	int t;
	std::cin >> t;
	while(t--)
	{
		int n;
		std::cin >> n;
		int** tree=(int**)malloc(sizeof(int*)*n);
		int** memoization = (int**)malloc(sizeof(int*)*n);
		for (int i = 1; i <=n ; ++i)
		{
			*(tree+i-1)=(int*)malloc(sizeof(int)*i);
			*(memoization+i-1)=(int*)malloc(sizeof(int)*i);
			for (int j = 0; j < i; ++j)
			{
				std::cin >> *(*(tree+i-1)+j);
				*(*(memoization+i-1)+j) = 0;
			}
		}
		std::cout <<  max_path(tree,0,0,n,memoization) << std::endl ;
	}
	return 0;
}