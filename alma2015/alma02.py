#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
from bisect import bisect_left as bisect

def arr_gen():
    arr = [0]*(15*9)
    for x in xrange(15):
        for y in xrange(9):
            arr[9*x + y] = (x + 1)*(10**x)

    for x in xrange(1, 15*9):
        arr[x] += arr[x - 1]

    return arr

def nthdigit(n):
    arr = arr_gen()
    while True:
        k = bisect(arr, n)
        if arr[k] == n:
            return k % 9 + 1
        diff = n - (10**(k // 9))*(k % 9 + 1)
        if (bisect(arr, diff) // 9) != (k // 9) - 1:
            return 0
        else:
            n = diff
    return k % 9

def main():
    print arr_gen()
    for _ in xrange(int(raw_input())):
        print nthdigit(map(int, raw_input().split())[1])

main()
