#include <bits/stdc++.h>
using namespace std;
#define usingcincout ios::sync_with_stdio(0);cin.tie(0);

template <class T>
void gint(T& n) {
    n = 0;
    int sign=1;
    register char c=0;
    while(c<33)
        c=getchar_unlocked();
    if (c=='-'){
        sign=-1;
        c=getchar_unlocked();
    }
    while(c>='0'&&c<='9'){
        n=(n<<3)+(n<<1)+(c-'0');
        c=getchar_unlocked();
    } n *= sign;
}
template <class T>
void dint(T& a) {
    char num[20];
    int i = 0;
    if(a < 0) {
        putchar_unlocked('-');
        a *= -1;
    }
    do {
        num[i++] = a%10 + 48;
        a /= 10;
    }  while (a != 0);
    i--;
    while (i >= 0)
        putchar_unlocked(num[i--]);
}
int main(int argc, char *argv[])
{
    int t;
    gint(t);
    while(t--)
    {
        int n;
        set<int> arr;
        gint(n);
        for (int x = 0; x < n; ++x)
        {
            int tmp;
            gint(tmp);
            pair<int, set<int>::iterator> it = arr.insert(tmp);
            dint(tmp - distance(arr.begin(), it.second));
        }
    }
    return 0;
}
