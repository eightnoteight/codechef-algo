#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
from sys import stdin

def maxsub_dp(arr):
    # print arr
    if 1 not in arr:
        return 0, (0, 0)
    a = b = s = tmp = 0
    maxsum = 0
    for ind, x in enumerate(arr):
        tmp += x
        if maxsum < tmp:
            maxsum = tmp
            a = s
            b = ind + 1
        if tmp < 0:
            tmp = 0
            s = ind + 1
    return maxsum, (a, b)

def main():
    inp = stdin.read().split()
    for g in xrange(int(inp[0])):
        s = inp[g + 1].strip()
        l = []
        for c in s:
            if c == '0':
                l.append(1)
            else:
                l.append(-1)
        _, limits = maxsub_dp(l)
        # print _, limits
        bc = 0
        for x in xrange(limits[0]):
            if s[x] == '1':
                bc += 1
        for x in xrange(limits[0], limits[1]):
            if s[x] == '0':
                bc += 1
        for x in xrange(limits[1], len(s)):
            if s[x] == '1':
                bc += 1
        if limits[0] == limits[1]:
            bc -= 1
        print bc

main()
