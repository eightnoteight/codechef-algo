using namespace std;
#include <stdio.h>
#include <iostream>
inline int get_int(int n=0)
{
	
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();

	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
int main(int argc, char const *argv[])
{
	int t=get_int();
	while(t--)
	{
		int count=0;
		string pint;
		getline(cin, pint);
		for (int i = 0; i < pint.length(); ++i)
			if (pint[i]=='A'||pint[i]=='B'||pint[i]=='D'||pint[i]=='O'||pint[i]=='P'||pint[i]=='Q'||pint[i]=='R')
			{
				count++;
				if (pint[i]=='B')
					count++;
			}
		cout << count << endl;
	}
	return 0;
}