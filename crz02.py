#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin

def main():
    arr = [0]*(1000001)
    arr[0] = arr[1] = 1
    for x in xrange(2, 1000001):
        arr[x] = (arr[x - 1] + arr[x - 2]) % 1000000007
    dstream = map(int, stdin.read().split())
    for t in xrange(1, dstream[0] + 1):
        print arr[dstream[t]]

main()
