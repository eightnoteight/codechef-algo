#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin


def main():
    inp = iter(stdin.read().split())

    for _ in xrange(int(next(inp))):
        for s in sorted([next(inp) for _ in xrange(int(next(inp)))]):
            print s


main()