using namespace std;
#include <stdio.h>
#include <iostream>
inline long long int get_long_int(long long int n=0)
{
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();

	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
inline int get_int(int n=0)
{
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();

	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
int main(int argc, char const *argv[])
{
	int t=get_int();
	for (int i = 0; i < t; ++i)
	{
		long long int n=get_long_int();
		printf("%lld\n", (n/2)+1);
	}
	return 0;
}