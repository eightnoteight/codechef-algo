#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from math import ceil, sqrt
from sys import stdin
from itertools import imap

def main():
    def sieve(n):
        arr = [x for x in xrange(n)]
        for x in xrange(4, n, 2):
            arr[x] = 0
        for x in xrange(9, n, 6):
            arr[x] = 0
        arr[1] = 0
        for x in xrange(1, int(ceil((sqrt(n) + 1) / 6.0)) + 1):
            if arr[6*x - 1]:
                arr[(6 * x - 1) * (6 * x - 1): n: 2 * (6 * x - 1)] = \
                    [0]*int(ceil((n - (6 * x - 1)*(6 * x - 1)) \
                                 / float(2 * (6 * x - 1))))
            if arr[6*x + 1]:
                arr[(6 * x + 1) * (6 * x + 1): n: 2 * (6 * x + 1)] = \
                    [0]*int(ceil((n - (6 * x + 1)*(6 * x + 1)) \
                                 / float(2 * (6 * x + 1))))
        return filter(None, arr)

    primesum = [0]*1001
    for x in sieve(1001):
        primesum[x] += x
    for x in xrange(1, 1001):
        primesum[x] += primesum[x - 1]
    dstream = imap(int, stdin.read().split())
    for x in xrange(next(dstream)):
        print primesum[next(dstream)] - 100

main()
