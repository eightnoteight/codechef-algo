
for x in xrange(int(raw_input())):
    s = raw_input()
    length = len(s)
    freq1 = [0]*(26)
    freq2 = [0]*(26)
    a = ord('a')
    for x in xrange(length/2):
        freq1[ord(s[x])-a] += 1
    if length % 2:
        p = 1
    else:
        p = 0
    for x in xrange(length/2):
        freq2[ord(s[x+length/2+p])-a] += 1
    if freq1 == freq2:
        print 'YES'
    else:
        print 'NO'
