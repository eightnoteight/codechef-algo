#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <cstring>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <iterator>
using namespace std;
inline long int get_long_int(long int n=0)
{
	int sign=1;
	char c=0;
	while(c<33)
		c=getchar_unlocked();
	if (c=='-')
	{
		sign=-1;
		c=getchar_unlocked();
	}
	while(c>='0'&&c<='9')
	{
		n=(n<<3)+(n<<1)+(c-'0');
		c=getchar_unlocked();
	}
	return n*sign;
}
char* mul(char* num, int tip, char* result)
{
	int carry=0;
	for (int i = 0; i < strlen(num); ++i)
	{
		result[i]	= (((num[i]-'0')*tip)+carry)/10;
		carry		= (((num[i]-'0')*tip)+carry)%10;
	}
	return result;
}
char* append_zero(char* number, int zeros, char* nstring)
{
	for (int i = strlen(number)-1 + zeros; i >= zeros ; ++i)
		nstring[i]=number[i-zeros];
	for (int i = 0; i < zeros; ++i)
		nstring[i]='0';
	return nstring;
}
char* add_num_strings(char* a, char* b, char* res)
{
	int len1=strlen(a);
	int len2=strlen(b);
	char carry[200];
	for (int i = 0; i < 200; ++i)
		carry[i]='0';
	reverse(a, a+strlen(a));
	reverse(b, b+strlen(b));
	int i=0;
	while(len1 && len2)
	{
		len2--; len1--;
		int tempres=a[i]+b[i]+carry[i]-'0'-'0'-'0';
		res[i] = (tempres%10) + '0';
		carry[i+1] += int(tempres/10) ;
		i++;
	}
	while(len1 > 0)
	{
		len1--;
		int tempres=a[i]+carry[i]-'0'-'0';
		res[i] = (tempres%10) + '0';
		carry[i+1] += int(tempres/10) ;
		i++;
	}
	while(len2 > 0)
	{
		len2--;
		int tempres=b[i]+carry[i]-'0'-'0';
		res[i] = (tempres%10) + '0';
		carry[i+1] += int(tempres/10) ;
		i++;
	}
	if (carry[i]!='0')
	{
		res[i]=carry[i++];
	}
	res[i]='\0';
	reverse(res, res+strlen(res));
}
char* multip(char* aaa, char* bbb)
{
	char result[200];
	append_zero(mul(bbb, aaa[0]-'0', result), 0, result);
	for (int i = 1; i < strlen(aaa); ++i)
	{
		char temp[200];
		append_zero(mul(bbb, aaa[i]-'0', temp), i, temp);
		add_num_strings(result, temp, result);
	}
}
int main(int argc, char const *argv[])
{
	char a[200]="120";
	char b[200]="1";
	cout << multip(a,b);
	return 0;
}